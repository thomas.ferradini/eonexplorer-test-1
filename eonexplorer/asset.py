# MIT License - Eonpass 2020

# Methods for Block
import subprocess
from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException
import eonexplorer.config as config
import os

# def put(payload):
#     try:
#         blocks = "1"
#         if(payload and payload['blocks']): blocks = str(payload['blocks'])
#         #python 3.6 stdout=PIPE, stderr=PIPE
#         #on EC2 multiple args don't work, probably it is some convertion to str messing, it works as a single command string though
#         if os.name == 'nt':
#             result = subprocess.run([config.CLI_PATH, "-datadir="+ config.DATADIR_PATH, "generatetoaddress",blocks,config.BASE_ADDRS], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True, shell=True)
#         else:
#             result = subprocess.run([config.CLI_PATH+" -datadir="+config.DATADIR_PATH+" generatetoaddress "+blocks+" "+config.BASE_ADDRS], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True, shell=True)
#         return result.stdout
#
#     except Exception as general_exception:
#         print("An Exception occured: " + str(general_exception))
#         return "Internal Error", 500

def get_all():
    try:
        rpc_connection = AuthServiceProxy("http://%s:%s@%s:%s"%(config.RPC_USER, config.RPC_PASSWORD, config.RPC_HOST, config.RPC_PORT))
        asset=rpc_connection.listissuances()
        return  asset
    except JSONRPCException as json_exception:
        print("A JSON RPC Exception occured: " + str(json_exception))
        return "Internal Error", 500
    except Exception as general_exception:
        print("An Exception occured: " + str(general_exception))
        return "Internal Error", 500
