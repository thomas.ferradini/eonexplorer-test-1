# MIT License - Eonpass 2020

# Methods for TX
from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException
import eonexplorer.config as config

def get(txid):
    rpc_connection = AuthServiceProxy("http://%s:%s@%s:%s"%(config.RPC_USER, config.RPC_PASSWORD, config.RPC_HOST, config.RPC_PORT))
    try:
        rawTx=rpc_connection.getrawtransaction(txid, 1)
        #unblind to see if you have enought value
        unblindedTx = rpc_connection.unblindrawtransaction(rawTx['hex'])
        decodedUnblindedTx = rpc_connection.decoderawtransaction(unblindedTx['hex'])
        return decodedUnblindedTx;
    except JSONRPCException as json_exception:
        print("A JSON RPC Exception occured: " + str(json_exception))
        #if this is the origin transction, only gettransaction can be called
        try:
            tx = rpc_connection.gettransaction(txid)
            return tx
        except JSONRPCException as json_exception:
            print("A JSON RPC Exception occured: " + str(json_exception))
            return "Internal Error", 500    
    except Exception as general_exception:
        print("An Exception occured: " + str(general_exception))
        return "Internal Error", 500

def get_blinded(txid):
    try:
        rpc_connection = AuthServiceProxy("http://%s:%s@%s:%s"%(config.RPC_USER, config.RPC_PASSWORD, config.RPC_HOST, config.RPC_PORT))
        rawTx=rpc_connection.getrawtransaction(txid, 1)
        return  rawTx
    except JSONRPCException as json_exception:
        print("A JSON RPC Exception occured: " + str(json_exception))
        return "Internal Error", 500
    except Exception as general_exception:
        print("An Exception occured: " + str(general_exception))
        return "Internal Error", 500
