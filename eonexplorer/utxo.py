# MIT License - Eonpass 2020

# Methods for UTXO
from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException
import eonexplorer.config as config

def put(payload):
    value = payload['value']
    #create a new UTXO
    try:
        #get the max value UTXO
        utxos = getAll();
        maxvalue = 0;
        txid = '';
        vout='';
        for utxo in utxos:
            currentvalue = float(utxo.get('value'))
            if currentvalue > maxvalue:
                txid=utxo.get('txid')
                vout=utxo.get('vout')
                maxvalue=currentvalue
        if(value>maxvalue):
            return "Not enough value", 403
        #spent the highest value output to create a new utxo
        rpc_connection = AuthServiceProxy("http://%s:%s@%s:%s"%(config.RPC_USER, config.RPC_PASSWORD, config.RPC_HOST, config.RPC_PORT))
        address = rpc_connection.getnewaddress();
        ins = [
            {
                'txid': txid, 
                'vout': vout
            }
        ]
        outs = {
            ""+address: value
        }
        rawTx = rpc_connection.createrawtransaction(ins, outs)
        fundedTx = rpc_connection.fundrawtransaction(rawTx) #fix fees
        blindedTx = rpc_connection.blindrawtransaction(fundedTx["hex"])
        signedTx = rpc_connection.signrawtransactionwithwallet(blindedTx)
        sentTx = rpc_connection.sendrawtransaction(signedTx['hex'])
        #tx is submitted to mempool, next block will incorporate it
        return (sentTx)
    except JSONRPCException as json_exception:
        print("A JSON RPC Exception occured: " + str(json_exception))
        return "Internal Error", 500
    except Exception as general_exception:
        print("An Exception occured: " + str(general_exception))
        return "Internal Error", 500

def getAll():
    #get all the UTXOs of the current wallet
    try:
        rpc_connection = AuthServiceProxy("http://%s:%s@%s:%s"%(config.RPC_USER, config.RPC_PASSWORD, config.RPC_HOST, config.RPC_PORT))
        utxos = rpc_connection.listunspent(1)
        compactUtxos = []
        #extract the txids with unspent outputs
        for utxo in utxos:
            compactUtxos.append({'txid': utxo['txid'], 'vout': utxo['vout'], 'value':str(utxo.get('amount'))})    
        return compactUtxos   
    except JSONRPCException as json_exception:
        print("A JSON RPC Exception occured: " + str(json_exception))
        return "Internal Error", 500
    except Exception as general_exception:
        print("An Exception occured: " + str(general_exception))
        return "Internal Error", 500
    